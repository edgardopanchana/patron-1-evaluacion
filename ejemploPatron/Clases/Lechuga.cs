﻿using System;
using ejemploPatron.Interfaces;

namespace ejemploPatron.Clases
{
    public class Lechuga: PatronHamburguesa
    {
        public Lechuga(IHamburguesa hamburguesa): base(hamburguesa)
        {

        }

        public override string getDescripcion()
        {

            return  this.hamburguesa.getDescripcion() +"Lechuga\n" ;
        }
    }
}
