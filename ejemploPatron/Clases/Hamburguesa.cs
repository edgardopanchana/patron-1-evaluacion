﻿using System;
using ejemploPatron.Interfaces;

namespace ejemploPatron.Clases
{
    public class Hamburguesa : IHamburguesa
    {
        public string getDescripcion()
        {
            return "Carne\nPan\n";
        }
    }
}
