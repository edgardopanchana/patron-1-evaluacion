﻿using System;
using ejemploPatron.Interfaces;

namespace ejemploPatron.Clases
{
    public class Tomate: PatronHamburguesa
    {
        public Tomate(IHamburguesa hamburguesa):base(hamburguesa)
        {
        }

        public override string getDescripcion()
        {
            return this.hamburguesa.getDescripcion() + "Tomate\n";
        }
    }
}
