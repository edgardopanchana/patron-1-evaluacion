﻿using System;
using ejemploPatron.Interfaces;

namespace ejemploPatron.Clases
{
    public abstract class PatronHamburguesa:IHamburguesa
    {

        protected IHamburguesa hamburguesa;

        public PatronHamburguesa(IHamburguesa hamburguesa) { this.hamburguesa = hamburguesa; }        
        public abstract string getDescripcion();
        
    }
}
