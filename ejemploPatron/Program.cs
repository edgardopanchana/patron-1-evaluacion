﻿using System;
using ejemploPatron.Clases;
using ejemploPatron.Interfaces;

namespace ejemploPatron
{
    class Program
    {
        static void Main(string[] args)
        {
            Hamburguesa normal = new Hamburguesa();            Lechuga hamburguesaConLechuga = new Lechuga(normal);                        Lechuga hamburguesaConDobleLechuga = new Lechuga(hamburguesaConLechuga);            Tomate hamburguesaConTomate = new Tomate(hamburguesaConDobleLechuga);

            Console.WriteLine(hamburguesaConTomate.getDescripcion());
        }
    }
}
